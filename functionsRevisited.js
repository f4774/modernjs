// default paramters

function greetings(userName = 'user')
{
    console.log(`hi ${userName}`);
    
}

greetings('Rajesh'); // hi Rajesh

greetings(); // no error hi undefined if there is no default value set for parameter


// call back functions

/*function AskUser(question) // Version 1
{
    if(confirm(question))
    {
        console.log("YES !!!! LEARNING JS ADVANCED CONCEPTS!!!");
    }
    else
    {
        console.log("NO !!!! ... JS IS BORING !!!");
    }
}
*/

/*
function AskUser(question) // Version 2
{
    if(confirm(question))
    {
        successPath();
    }
    else
    {
        rejectPath();
    }
}*/

function AskUser(question, success, reject ) // Version 3
{
    if(confirm(question)) // "OK" button in confirmation box sends true
    {
        alert('successpath function definition as a string::' + success);
        success();
    }
    else // "Cancel" button in confirmation box sends false
    {
        reject();
    }
}


AskUser('Are u learning call back functions in JS', successPath, rejectPath);


function successPath()
{
    console.log("YES !!!! LEARNING JS ADVANCED CONCEPTS!!!");
}

function rejectPath()
{
    console.log("NO !!!! ... JS IS BORING !!!");
}



