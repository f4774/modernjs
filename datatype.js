"use strict" // will impose latest JS stds

// primitive & non-primitive

let a = 1; // 4 bytes
//b = 1,2 // error
let username = 'Divash' // 8 bytes

// Dynamically typed 

/*
String
Number
bigint
boolean
symbol

null 
undefined

Arrays , objects , map, set 
*/

// String
let userName = 'water bottle';
console.log(userName);

//  string concatenation
const planetName = 'Earth';
const galaxyName = 'Milky Way';

const output = 'Planet ' + planetName + ' is located in the galaxy' + galaxyName + ' in our universe '; // plus operator 
console.log(output);

// back tick (modern JS)
const backtickOutput = `Planet ${planetName}  is located in the galaxy ${galaxyName} in our universe`;
console.log(backtickOutput);

// More than one line scenario
const htmlcode = `
<div style="background-color='red'">
    <p>Learning string manupulation!!!</p>
<div>
`;

console.log(htmlcode);

console.log('Length of htmlcode variable is ' + htmlcode.length);

const productName = 'SalesForce is awesome';
console.log(productName.length);
console.log('Get me first character ' + productName[5]);
console.log('Get me first character ' + productName.charAt(5));

console.log('Is Sales exist??? ' + productName.indexOf('aaales'));
console.log('Is Sales exist??? ' + productName.includes('Sales'));

/*
Cheat book:
String fucntions:

https://www.shortcutfoo.com/app/dojos/javascript-strings/cheatsheet

*/


// Type of

var evennumber = 2; // let or const or var ??? - var
let pi  = 3.14;

const sumup = evennumber + pi;

console.log(typeof evennumber);
console.log(typeof pi);
console.log(sumup);


// comparision 
const x = 1;
const y = '1';

console.log(x == y);
console.log(x === y);
/*
 == :: compares only the value
 ==== :: compares value and the datatype

*/


// Implicit conversion

const xy = x + y;
console.log(xy);

const out1 = true + 1 + '1'; // 1+ 1+'1' => 21 implicit conversion
console.log(out1);


const out2 = true + '1' + '1'; // no implicity conversion cos we dont deal with numbers 
console.log(out2);

const out3 = 2 + true + '1' + '1'; // 
console.log(out3);







