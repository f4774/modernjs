// Objects, Arrays & functions
// nullish operator

// Method 1  : Legacy 
function squareRoot(input)
{
    return input * input ;
}

//call function
const out = squareRoot(2);
console.log(`The output is ${out}`);

function greetings()
{
    return "hello!!"; 
}
const outputfunction = greetings; // assign function to a variable 

console.log('greetings::' + greetings); // treat this as a string an print entire function
console.log('greetings()::' + greetings()); // it will execute only when () is present 

console.log('outputfunction ::' + outputfunction); 
console.log('outputfunction() ::' + outputfunction()); // callthe variable as function 
 
// Method 2: Nameless

const squareRoot_V2 = function (input)
{
    return input * input;
};

console.log(squareRoot_V2(5));

// Method 3: Fat arrow or arrow function

let squareRoot_V3 = (input) => { 

    return input * input;
};
console.log(squareRoot_V3(10));

let add = (x,y,greet) =>
{
    console.log('inside add ::' + greet());
    //greet();
    return x + y;
}
console.log(add(5,5, greetings)); // passing function as a parameter

// Objects

// store in a keyed collections or key : value pair
let apiName_V1 = new Object(); // Method 1 : To create an object
let apiName_V2 = {}; // Method 2: To create an object

let apiNames = {
    "newsapi" : 'https://news.org/api', // key - value pair 1
    "textAnalysis" : 'https://text.rapdi.com/api', //// key - value pair 1
    key: 1234,
    '50': 'concurrentUsers',
    "Time out value": '50secs' 
};

console.log(apiNames.newsapi);
console.log(apiNames.textAnalysis);
console.log('Key:: ' + apiNames.key);
console.log('value of Key 50:: ' + apiNames["50"]);
console.log('value of time out:: ' + apiNames["Time out value"]);

function createNewAPI(apiname, url, timeout)
{
    // create an object
    let objApi = {

        apiname: apiname,
        url :url,
        timeout: timeout
    };

    return objApi; // return the object
}

// call the function
const outApi = 
createNewAPI('Random User', 'https://rapidapi.com/randomuser/api', 50);

console.log('outApi ::' + outApi); // [object object]

// use JSon. stringfy to convert object to a string
console.log('Convert object to a string ::' + JSON.stringify(outApi));

// Tip: USe JSON.Parse to convert string to object

// for loop
// Use for ...in to loop thru obj
for(keyValue in outApi)
{
    console.log('Key is ' + keyValue); // wil get me key
    console.log('Value is ' + outApi[keyValue]); // use the key and get value
}

// check if a key exists in object
const keytoSearch = 'url';
console.log(keytoSearch in outApi); // returns true if key is present in the object 

// Cheat book for objects

// Arrays
let arr_V1 = new Array(); // Method 1 : To create an array
let arr_V2 = []; // Method 2: To create an array

// add items into array directly or using inbuilt function

// Option 1: direct items additon
let SalesforceFeatures = ['Code builder', 'Flow', 'Eninstein Anlaysis', 1, {apiname: 'gitlab'}];
console.log(SalesforceFeatures);
console.log(SalesforceFeatures[4]);

// Option 2: using inbuilt functions
SalesforceFeatures.push('Quip');
SalesforceFeatures.push('Shield');

console.log(SalesforceFeatures.length);

SalesforceFeatures[1000] = 'Sales Optimizer';

SalesforceFeatures.length = 2; // to truncate items in the array
console.log(SalesforceFeatures);




/*
Cheat sheet:
https://www.shortcutfoo.com/app/dojos/javascript-arrays/cheatsheet

https://dev.to/vincenius/javascript-object-functions-cheat-sheet-48nn
*/

