// function call back
// ES 6 Stds - Promise 
// ES 2017 - Async / Await

// Make a call to DB and then return the list
// Say it takes 5 secs for the DB to return the data

function getAPIs() {

    const ApiLst = [
        { apikey: 1, apiName: "NewsAPI.", url: 'https://gnews.com/api' },
        { apikey: 2, apiName: "dogsAPI", url: 'https://dogs.com/api' },
        { apikey: 3, apiName: "gitHubDPAPI", url: 'https://gituser.com/api' }
    ];

    return ApiLst;
}


const lstAPIs = setTimeout(getAPIs, 5000);
console.log("lstAPIs ::" + JSON.stringify(lstAPIs));
console.log("I dont mind if settime out is working or not");
console.log("but i m stil concerned abt the oupput !!!");

// Promise syntax:

// Executor code
let promiseObj = new Promise(
    (resolve, reject) => {

        // long running code to be executed async way will be added here !!!

        if ("ok to proceed and done with the work !!!")
            Promise.resolve("Success !!! all  is done !!!");
        else
            Promise.reject("Error !!! Something went wrong !!! Pls check!!!");

    }
);

//Consumer code:
promiseObj.then(onSuccess, onFailure);

function onSuccess() {
    console.log('handle succcess scenario... ');
}

function onFailure() {
    console.log('handle error scenario... ');
}



// get APIs using promise

// executor code
function readAPIsFromDB() {

    return promiseObj = new Promise(
        (resolve, reject) => {

            // Introduce delay of 5 secs before api list is being returned
            const ApiLst = [
                { apikey: 1, apiName: "NewsAPI.", url: 'https://gnews.com/api' },
                { apikey: 2, apiName: "dogsAPI", url: 'https://dogs.com/api' },
                { apikey: 3, apiName: "gitHubDPAPI", url: 'https://gituser.com/api' }
            ];

            if (1 == 1)
                setTimeout(() => { resolve(ApiLst); }, 5000); // success or fulfilled state
            else
                setTimeout(() => { reject('Error'); }, 5000); // error state or reject state
        }

    );
}

// consumer code
const listAPIs = readAPIsFromDB();
listAPIs.then(onSuccess, onFailure);

function onSuccess(ApiList, returncode) {
    console.log('HTTP return code::' + returncode);
    console.log('output from promise::' + ApiList);
}

function onFailure(strMsg) {
    console.log('output from promise::' + strMsg);
}

/*
listAPIs
.then(onSuccess, onFailure) // get apis
.then(onSuccessAPIName,onFailureAPIName) // get API Subscription
.then(onSuccessSubscruptionLimit,onFailureSubscruptionLimit) // get limts*/

// async / Await


//Syntax

//let asyncResult = await func(); // You can use await keyword only inside async function

async function showAllAPis() // use async keywork to denote async program
{

    try {

        let asyncResult = await readAPIsFromDB(); // 5 secs delayed
        let asynREsult2 = await longRunningCode2();
        let asynREsult3 = await longRunningCode3();

        console.log('APIs list from async method::::' + JSON.stringify(asyncResult));  // this line waits for the above to complete(5 secs) then executes
        
    }
    catch(error) {
        console.log('Error::' + error);
    }
}

const ApiList = showAllAPis();


async function SayHi()
{
    return 'hi'; // works fine 
    //return Promise.resolve('Hi');  // works fine  
}


let sayHello= async () => 'Hello there !!!';

let sayHello_1 = async () => { return 'Hello there !!!'};

// when there is one line inside arrow function and also its a return statement then no need to add curly braces