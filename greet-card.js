class GreenCard extends HTMLElement
{
    constructor() {
        super();
        this.shadowroot = this.attachShadow({mode:'open'}); // creates dom for element
    }

    connectedCallback()
    {
        const lang = this.getAttribute('language');
        const showImg = this.getAttribute('image');

        let msg, imgurl;
        switch(lang)
        {
            case "en-in":
                msg = `Good morning`
                imgurl = showImg ? 'https://cdn-icons-png.flaticon.com/128/706/706297.png' : '';
                break;
            case "fr":
                msg = 'bonjour' 
                imgurl = showImg ? 'https://cdn-icons-png.flaticon.com/128/3153/3153851.png' : '';
                break;
            case "gr":
                msg = "guten tag"
                imgurl = showImg ? 'https://cdn-icons-png.flaticon.com/128/3617/3617478.png' : '';
                break; 
        }
            
        //this.shadowroot.innerHTML = msg;
        
        this.shadowroot.innerHTML = `
        <div>
            <p style='text-transform: uppercase'>${msg}</p>
            <img src= ${imgurl}></img>
        </div>
        `;
    }

}

customElements.define("greet-card", GreenCard);