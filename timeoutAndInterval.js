// setTimeOut - execute code once afetr a particular interval of time
// setInterval - execute code in a particular interval 


function greetings()
{
    console.log('Hi there - I m getting executed after 8 secs!!!'); 


}

console.log('Start here !!!');
setTimeout(greetings, 8000); // runs the code after 8 secs
//setInterval(greetings, 3000); // run on every 3 secs
console.log('End here !!!')