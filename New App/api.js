/*const topics = ['breaking-news', 'world', 'nation', 'business', 'technology', 'entertainment', 'sports', 'science' , 'health'];
const gnewsapiurl = 'https://gnews.io/api/v4/top-headlines?token=d3b677d3b44ba5c8350988fd89ac8fe6&lang=en&country=in&topic=science';
export { gnewsapiurl, topics }*/


// Import sample data from newsData.js
import { newsData_breakingNews, newsData_world, newsData_Science } from "./newsData.js";

// Module to manage news api and sample data access var & function
const topics_sample = ['breaking-news', 'world', 'science'];

function getNewsSampleData(newsTopic) {
    let newsData; // var to hold news sampple articles
    switch (newsTopic) {
        case 'breaking-news':
            newsData = newsData_breakingNews;
            break;
        case 'world':
            newsData = newsData_world;
            break;
        case 'science':
            newsData = newsData_Science;
            break;
        default:
            newsData = newsData_breakingNews;
            break;
    }

    return newsData;

};


const topics = ['breaking-news', 'world', 'nation', 'business', 'technology', 'entertainment', 'sports', 'science', 'health'];
const gnewsapiurl = 'https://gnews.io/api/v4/top-headlines?token=d3b677d3b44ba5c8350988fd89ac8fe6&lang=en&country=in&topic=science';

function getNewsApiURL(topicName) {
    let newsUrl; // var to hold newsurl based on the topic 
    
    const strtopicName = topicName ?? "breaking-news"; // if topic name is null or undefine then it will pick up default value  
    newsUrl = `https://gnews.io/api/v4/top-headlines?token=d3b677d3b44ba5c8350988fd89ac8fe6&lang=en&country=in&topic=${strtopicName}`;

    return newsUrl;
}

export { getNewsSampleData, topics, getNewsApiURL }

