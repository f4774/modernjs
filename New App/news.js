//import { newsData, newsApiLimtPerHour, WhoAmI } from "./newsData.js";
import { getNewsSampleData, topics, getNewsApiURL } from "./api.js";




// refresh news content for every 5 secs
// pick up new topic for evey 5 secs and get data to display

var topicCounter = 1;
fetchNews(); // show default news on load
setInterval(displayNews, 8000); // run every 10 secs 

function displayNews() {

    topicCounter++;
    console.log('TopicCounter: ' + topicCounter);

    if (topicCounter == topics.length)
        topicCounter = 0;

    // pass news topic to fetch newws
    const newsTopic = topics[topicCounter];

    fetchNews(newsTopic) // send topic
}

async function fetchNews(newsTopic) {

    try {

        // call api module to get api URL
        const newsURL = getNewsApiURL(newsTopic);

        // Use JS fetch method to make a call to an URL
        // use it when URL is going to return JSON as output
        const response = await fetch(newsURL);
        const outputJson = await response.json();

        // find newsheader
        const newsBanner = document.querySelector('#newsHeader');
        newsBanner.innerHTML = newsTopic ?? "Breaking News";

        // Find template
        const elemNewsContainner = document.querySelector('#newsContainner'); // Step 4
        const templateNewsCard = document.querySelector('#news-card'); // Step 1

        elemNewsContainner.innerHTML = ""; // reset news containner before loading news from new  topic 

        // Step 1: Find the news card template - DONE
        // Step 2: Loop thru news Data and clone template content 
        // Step 3: For each news article, read values and assign to elements inside template
        // Step 4: Find the news containner 
        // Step 5: Add each card dynamically into news containner


        outputJson.articles.map(article => {

            // Copy or clone the content for further processing
            const templateContent = templateNewsCard.content.cloneNode(true); // Step 2

            // Find elements - Step 3
            const elemNewsImage = templateContent.querySelector('#newsimg');
            const elemNewsUrl = templateContent.querySelector('#newsurl');
            const elemTitle = templateContent.querySelector('#title');
            const elemDesc = templateContent.querySelector('#description');

            // Assign Values by reading array of objects - Step 3
            elemNewsUrl.href = article.url;
            elemNewsImage.src = article.image;
            elemTitle.innerHTML = article.title;
            elemDesc.innerHTML = article.description;

            elemNewsContainner.appendChild(templateContent); // Step 5


        });
    }
    catch (error) {
        console.log(error);
    }




}

/*
// Find elements
const elemNewsImage = document.getElementById('newsimg'); // method 1
const elemNewsUrl = document.getElementById('newsurl');
const elemTitle = document.querySelector('#title'); // method 2
const elemDesc = document.querySelector('#description'); // method 2

// Assign Values by reading array of objects
const newsid = newsData[0].newid;
elemNewsUrl.href = newsData[0].url;
elemNewsImage.src = newsData[0].urlToImage;
elemTitle.innerHTML = newsData[0].title;
elemDesc.innerHTML = newsData[0].description;

*/
// Loop thru array and show each new article in a separate card

// Html modules
// HTML templates - Does not render by the engine unless we invoke using JS 


