let person = {
    name: 'Navya',
    job: 'developer'
}
console.log(person.name);

//Option 1 : Use dot notation
person.languageToRead = 'English';

console.log('Person::' + JSON.stringify(person));

// Option 2: Use brackets
person['languageToWrite'] = 'Spanish';

console.log('Person::' + JSON.stringify(person));

// Option 3: Object.assign()
const newKeyValueToAdd = { 'programmingLanguage' : 'APEX', 'productsToWorkWith' : 'Sales Cloud & Service Cloud'};
Object.assign(person,newKeyValueToAdd);
console.log('Person - Option 3 ::' + JSON.stringify(person));

// Option 4: Spread operator ES6
const newKeyValuePair = { 'Yrs Of Experience' : 5};

let obj = {...person, ...newKeyValuePair};
console.log('Person - Option 4 ::' + JSON.stringify(obj));



 