/* ?? */
// it treats null or undefined 

const getRandomImage = (imgURL) => {

    console.log('value of imgURL is ' + imgURL);
    if(imgURL !=null)
    {
        console.log('Fetch the image and return back response');
    }
    else
    {
        console.log('Send a default image as response!!');
    }

};

//getRandomImage('https://www.randomimgae/1');
//getRandomImage();

const getRandomImage_refactored = (imgURL) => {

    console.log('value of imgURL is ' + imgURL);
    const output = imgURL ?? 'Send a default image as response!!';
      
    console.log(output);
    
};

getRandomImage_refactored('https://www.randomimgae/refactored');
getRandomImage_refactored();


let divheight;
let divWidth = 50;

console.log(divheight ?? divWidth ?? 100); // default to 100 cos divHeight is not given

/*
polyfills - to make latest stds work in unsupported browser
*/