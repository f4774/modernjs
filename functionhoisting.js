greeting(); // works fine with function declaration (hoisting)

//apiName(); // REference error . Won't work u cannot call an expression before it is declared 

const apiName = function () // function expression : Nameless
{
    let newsApi = { 'name': 'newsApi.org', 'isOpenApi' : false};
    console.log(newsApi.name + ' - ' + newsApi.isOpenApi);

};

//dbConnectionDetails(); // REference error . Won't work u cannot call an expression before it is declared 


// arrow function 
const dbConnectionDetails = (sourceDB) => {

    console.log(`${this.sourceDB} info`);
};



function greeting() // function declaration
{
    console.log('Hello!!!')
}

apiName(); // works fine
dbConnectionDetails(); // works fine

